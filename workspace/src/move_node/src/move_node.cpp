#include "ros/ros.h"
#include "geometry_msgs/Twist.h"

int	main(int ac, char **av)
{
	ros::init(ac, av, "move_node");
	ros::NodeHandle n;
	ros::Publisher pub =n.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);
	ros::Rate loop_rate(1);
	for(int i = 0; i < 20; i++)
	{
		geometry_msgs::Twist pos;
		pos.linear.x = 2;
		pos.angular.z = 0;
		pub.publish(pos);
		loop_rate.sleep();
	}
	ros::spinOnce();
return (0);
	
}
