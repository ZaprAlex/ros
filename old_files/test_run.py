#!/usr/bin/python
import rospy
from sensor_msgs.msg import LaserScan

def callback(data):
    print data.ranges[0]

rospy.init_node("move_node", anonymous=True)
pub = rospy.Subscriber('/scan', LaserScan, callback)
