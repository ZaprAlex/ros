#!/usr/bin/python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

range = list()
def callback(data):	
    #rospy.init_node("move_node", anonymous=True)
    print data.ranges[0]
    #range.append(data.ranges[0])
    pub = rospy.Publisher('/cmd_vel', Twist , queue_size=1000)
    hello_str = Twist()
    if data.ranges[0] < 1.0 or data.ranges[0] > 10:
        hello_str.linear.x = 0
        hello_str.angular.z = 0
    else:
        hello_str.linear.x = 50
        hello_str.angular.z = 0
    pub.publish(hello_str)


def move():
    rospy.init_node("read_node", anonymous=True)
    rate = rospy.Rate(50) # 10hz
    sc = rospy.Subscriber('/scan', LaserScan, callback)
    rate.sleep()
    rospy.spin()

move()
