#!/usr/bin/python
import rospy
import math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

def stop_move():
    pub = rospy.Publisher("/cmd_vel", Twist , queue_size=1000)
    hello_str = Twist()
    hello_str.linear.x = 0
    hello_str.linear.y = 0
    hello_str.linear.z = 0
    hello_str.angular.x = 0
    hello_str.angular.y = 0
    hello_str.angular.z = 0
    pub.publish(hello_str)

def go_ahead():
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    move = Twist()
    move.linear.x = -10
    move.angular.x = 0
    move.angular.y = 0
    move.angular.z = 0
    pub.publish(move)

def turn():
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    move = Twist()
    move.linear.x = 0
    move.angular.x = 0
    move.angular.y = 0
    move.angular.z = abs(2 * math.sin(45))
    pub.publish(move)


def callback(data):
    rate = rospy.Rate(10)
    stop = 0
    i = 154
    j = 206
    while i < 180:
        print("in while")
        if data.ranges[i] < 0.45 and data.ranges[i] != float('inf') or data.ranges[j] < 0.45 and data.ranges[j] != float('inf'):
	    print("too close")
            stop = 1
       	    print(data.ranges[i])
	    print(data.ranges[j])
            break
        i += 2
        j -= 2
    if stop == 1:
        print('turn')
        stop_move()
	turn()
    else:
        print('go_ahead')
        go_ahead()
    rate.sleep()


def move():
    rospy.init_node("read_node", anonymous=True)
    sc = rospy.Subscriber('/scan', LaserScan, callback)
    rospy.spin()

move()
