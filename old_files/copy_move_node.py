#!/usr/bin/python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

def go_ahead():
#    rospy.init_node("go_ahead", anonymous=True)
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    rate = rospy.Rate(10)
    move = Twist()
    rate.sleep()
    move.linear.x = 50
    move.angular.z = 0
    pub.publish(move)

def turn(gr):
 #   rospy.init_node("turn", anonymous=True)
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    rate = rospy.Rate(10)
    move = Twist()
    rate.sleep()
    move.linear.x = 0
    if gr > 0:
        move.angular.z = -0.75
    else
        move.angular.z = 0.75 
    pub.publish(move)

range = list()
def callback(data):	
   # rospy.init_node("move_node", anonymous=True)
    print data.ranges[0]
    #range.append(data.ranges[0])
    i = 0
    j = 359
    stop = 0
    while i < 26:
        if data.ranges[i] < 0.4 and data.ranges[i] != float('inf') or data.ranges[j] < 0.4 and data.ranges[j] != float('inf'):
            stop = 1
	    break
	i += 2
	j -= 2
   # pub = rospy.Publisher('/cmd_vel', Twist , queue_size=1000)
   # rate = rospy.Rate(10) # 10hz

#	hello_str = Twist()
    if stop == 1:
        ij = 90
        max = data.ranges[ij]
        gr = ij
        while ij < 270:
            if max < data.ranges[ij]
                max = data.ranges[ij]
                gr = ij
            ij += 30
        print('turn')
        gr -= 180
        turn(gr)
  #      hello_str.linear.x = 0
  #      hello_str.angular.z = 0
    else:
        print('go_ahead')
        go_ahead()
   #     hello_str.linear.x = 50
   #     hello_str.angular.z = 0
#	pub.publish(hello_str)
#	rate.sleep()


def move():
    rospy.init_node("read_node", anonymous=True)
    rate = rospy.Rate(10) # 10hz
    sc = rospy.Subscriber('/scan', LaserScan, callback)
    rate.sleep()
    rospy.spin()

move()
