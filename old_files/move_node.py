#!/usr/bin/python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

def stop_move():
    pub = rospy.Publisher("/cmd_vel", Twist , queue_size=1000)
    rate = rospy.Rate(10) # 10hz
    rate.sleep()
    hello_str = Twist()
    hello_str.linear.x = 0
    hello_str.linear.y = 0
    hello_str.linear.z = 0
    hello_str.angular.x = 0
    hello_str.angular.y = 0
    hello_str.angular.z = 0
    pub.publish(hello_str)
    rate.sleep()

def go_ahead():
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    rate = rospy.Rate(10)
    move = Twist()
    move.linear.x = -50
    move.angular.x = 0
    move.angular.y = 0
    move.angular.z = 0
    pub.publish(move)
    rate.sleep()

def turn():
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    rate = rospy.Rate(10)
    move = Twist()
    move.linear.x = 0
    move.angular.x = 1.62
    move.angular.y = 1.62
    move.angular.z = 1.62
    pub.publish(move)
    rate.sleep()
    rospy.sleep(1.62)

range = list()
def callback(data):
    stop = 0
    i = 154
    j = 206
    while i < 180:
        if data.ranges[i] < 0.55 and data.ranges[i] != float('inf') or data.ranges[j] < 0.55 and data.ranges[j] != float('inf'):
            stop = 1
       	    print(data.ranges[i])
	    print(data.ranges[j])
            break
        i += 2
        j -= 2
    if stop == 1:
        turn()
        stop_move()
        exit()
	stop = 0
    else:
        print('go_ahead')
        go_ahead()


def move():
    rospy.init_node("read_node", anonymous=True)
    rate = rospy.Rate(1) # 10hz
    sc = rospy.Subscriber('/scan', LaserScan, callback)
    #rospy.spin()
    rospy.sleep(2)

move()
