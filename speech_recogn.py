import urllib.request
import json
FOLDER_ID = "b1g9f4kqjp2a91njqkuq" # Идентификатор каталога
IAM_TOKEN = "CggaATEVAgAAABKABLB5WjVxQcMfh_dFuBlnqKlr8TxKreGjFj8Iq3Ge9kDJgqv2OSJwFXKMy4CpPEwHla7esuE8nNnaXClsdMRbavkhrjqyFjDH4n4pYYd2lJGt3CvG9ToqemIdMcDpmASGoupehFx8kEvbDonSp5gI4Mme4DBbn1NaDJe_9peQMaIucRu5bgXvvRLQuvA7ud2TQMlec4guZUOGwbmXZnp6MODQgsGXy2UWlBJfIRdpXfp8yynTsWW1usOEepoee6AKae_f5Q6WRPSC768U5wrB_l-sCu2gyghWw9Po5s_nPpRBe1Q7MnH7_aqnh2Wj1NnGy3W5dQv34J9CaVxRw_8GTl_r2j4tATIyl8iiu_x6umIXiC2QXGGzgLtVXS5ib48mZF7jk_L9bKiKNk91kjTYtKtuSZJ3IxVetDN14eem7HJRVk9qnYJ0icTkLZ_fAeYmU8ZKw1vQzpoTciS7xuCjsNGEVuOK1hkYOOUL9yHNyEMCT11iYaGhI1U3qK6e23cSkvbomzVKwvjQoA_eR8ELCd85W7KU894ZZuV-e1AoCugdN0Kp3WTU9Ml97D5d-2EGGwSSQRANJL3MrMiCl1tu_7_yoBboUCxvuKc82FbuKyLuoNVLbiENlFd2wmHBKz8jkVzp-jn69S8ghoyD4Wmj6HR-x4Q9fhlKCsLGv46Sm2_rGmYKIDMxYTMwMWMzNTY3YzQ5Nzk4MzQ4MmVkYzQ5NGU2MWQ4EIfzu-wFGMfEvuwFIiQKFGFqZW91N3E2dXAzbTlhY2ZnaGVrEgxyeWF0c3lnYW5rb3ZaADACOAFKCBoBMRUCAAAAUAEg9QQ"
with open("audio_2019-09-27_14-09-09.ogg", "rb") as f:
    data = f.read()

params = "&".join([
    "topic=general",
    "folderId=%s" % FOLDER_ID,
    "lang=ru-RU"
])

url = urllib.request.Request("https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?%s" % params, data=data)
url.add_header("Authorization", "Bearer %s" % IAM_TOKEN)

responseData = urllib.request.urlopen(url).read().decode('UTF-8')
decodedData = json.loads(responseData)

if decodedData.get("error_code") is None:
    print(decodedData.get("result"))