#!/usr/bin/python
import rospy
import math
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

def mean(lst):
    sum = 0.0
    for i in lst:
	sum += i
    return (sum / len(lst))

def stop_move():
    pub = rospy.Publisher("/cmd_vel", Twist , queue_size=1000)
    hello_str = Twist()
    hello_str.linear.x = 0
    hello_str.linear.y = 0
    hello_str.linear.z = 0
    hello_str.angular.x = 0
    hello_str.angular.y = 0
    hello_str.angular.z = 0
    pub.publish(hello_str)

def go_ahead():
    global mult
    mult = 1
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    move = Twist()
    move.linear.x = -10
    move.angular.x = 0
    move.angular.y = 0
    move.angular.z = 0
    pub.publish(move)

def turn(x):
    global mult
    pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1000)
    move = Twist()
    move.linear.x = 0
    move.angular.x = 0
    move.angular.y = 0
    if (x):
        move.angular.z = -0.75 * mult
    else:
        move.angular.z = 0.75 * mult
    pub.publish(move)
    mult += 0.05


def callback(data):
    rate = rospy.Rate(10)
    stop = 0
    i = 154
    j = 206
    while i < 180:
        if data.ranges[i] < 0.45 and data.ranges[i] != float('inf') or data.ranges[j] < 0.45 and data.ranges[j] != float('inf'):
	    print("too close")
            stop = 1
       	    print(data.ranges[i])
	    print(data.ranges[j])
            break
        i += 2
        j -= 2
    if stop == 1:
        print('turn')
	left = data.ranges[90:179]
	left_f = [value for value in left if value != float('inf')]
	#list(filter(lambda a: a != float('inf'), left))
	right = data.ranges[181:270]
	right_f = [value for value in right if value != float('inf')]
	#list(filter(lambda a: a != float('inf'), right))
	print("max left - " + str(max(left_f)) + " max right - " + str(max(right_f)))
	if (mean(left_f) > mean(right_f)):
	    print(1)
            turn(1)
	else:
	    print(0)
	    turn(0)
    else:
        print('go_ahead')
        go_ahead()
    rate.sleep()


def move():
    rospy.init_node("read_node", anonymous=True)
    sc = rospy.Subscriber('/scan', LaserScan, callback)
    rospy.spin()

mult = 1.0
move()
