#!/usr/bin/python
import rospy
from geometry_msgs.msg import Twist

def stop():
    rospy.init_node('stop_stop', anonymous=True)
    pub = rospy.Publisher("/cmd_vel", Twist , queue_size=1000)
    rate = rospy.Rate(10) # 10hz
    rate.sleep()
    hello_str = Twist()
    hello_str.linear.x = 0
    hello_str.linear.y = 0
    hello_str.linear.z = 0
    hello_str.angular.x = 0
    hello_str.angular.y = 0
    hello_str.angular.z = 0
    pub.publish(hello_str)
    rate.sleep()
    rospy.signal_shutdown("goodbye")

stop()
