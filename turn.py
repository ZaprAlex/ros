#!/usr/bin/python
import rospy
import math
from geometry_msgs.msg import Twist

rospy.init_node("move_node", anonymous=True)
pub = rospy.Publisher('/cmd_vel', Twist , queue_size=1000)
rate = rospy.Rate(1) # 10hz

rate.sleep()
hello_str = Twist()
hello_str.angular.x = 0
hello_str.angular.y = 0
hello_str.angular.z = 0.75
pub.publish(hello_str)
rate.sleep()

hello_str.angular.z = 0
pub.publish(hello_str)
rate.sleep()
